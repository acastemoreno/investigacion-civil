var gulp = require('gulp');
var sass = require('gulp-sass');
var wiredep = require('wiredep').stream;
var inject = require('gulp-inject');
var csso = require('gulp-csso');

gulp.task('styles', function(){

	var injectAppFiles = gulp.src('scss/styles/*.scss', {read: false});

	function transformFilepath(filepath) {
	    return '@import "' + filepath + '";';
	}

	var injectAppOptions = {
	    transform: transformFilepath,
	    starttag: '// inject:app',
	    endtag: '// endinject',
	    addRootSlash: false
	};

  	gulp.src('scss/main.scss')
  	.pipe(wiredep())
  	.pipe(inject(injectAppFiles, injectAppOptions))
    .pipe(sass())
    .pipe(csso())
    .pipe(gulp.dest('css'))
});
